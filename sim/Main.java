import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.util.Random;

public class Main extends JPanel
{
	static int option;
	static JFrame f2;
	static JFrame f3;
	Graphics g;
	CircleDrew cArr[];	
	LineDrew lArr[];
	static JFrame f;
	static Main m;
	Sell metriBordLine[][];
	int numberLine;
	int player; //1- red //2- blue

	int arrIndexCircle[];
	int curr;
	int j = 0;
	
	public int botPlayer()
	{
		int flag = 0;
		int i = 0;
		int x1 = 0;
		int y1 = 0;
		for(i = 0; i < 6; i++)
		{
			
			j = i+1;
			if(1 == 1)
			{
				if(j == 6)
				{
					j = 0;
				}
				if(metriBordLine[i][j].val == 0 && metriBordLine[j][i].val == 0)
				{
					metriBordLine[i][j].val = 1;
					metriBordLine[j][i].val = 1;
					metriBordLine[i][j].player = 2;
					int indexLine1 = metriBordLine[i][j].indexLine;//fix this line
					lArr[indexLine1].color(2);
				
										
					if(isWin(2) == 1)
					{
						x1 = i;
						y1 = j;
						metriBordLine[i][j].val = 0;
						metriBordLine[j][i].val = 0;
						metriBordLine[i][j].player = 0;
						int indexLine2 = metriBordLine[i][j].indexLine;//fix this line
						lArr[indexLine2].color(0);
						
					}
					else
					{
						Main.this.repaint();	
						flag = 1;						
						break;
					}
				}
			}
		}

		

		if(flag != 1)
		{
			for(i = 0; i < 6; i++)
			{
				for(int j = 0; j < 6; j++)
				{
					if(metriBordLine[i][j].val == 0 && metriBordLine[j][i].val == 0 && flag == 0)
					{
						//System.out.println("0000000000000000000");
						metriBordLine[i][j].val = 1;
						metriBordLine[j][i].val = 1;
						metriBordLine[i][j].player = 2;
						int indexLine1 = metriBordLine[i][j].indexLine;//fix this line
						lArr[indexLine1].color(2);
					
						if(isWin(2) == 1 && flag == 0)
						{
							x1 = i;
							y1 = j;
							metriBordLine[i][j].val = 0;
							metriBordLine[j][i].val = 0;
							metriBordLine[i][j].player = 0;
							int indexLine2 = metriBordLine[i][j].indexLine;//fix this line
							lArr[indexLine2].color(0);
						}
						else
						{
							Main.this.repaint();	
							flag = 1;					
							return 0;
						}					
					}
				}
			}
		}

		System.out.println("flag: " + flag);
		return 1;
	}

	public int isWin(int playerNum)
	{	
		int a[][]=new int[6][6];
		int c[][]=new int[6][6]; 
		int d[][]=new int[6][6]; 
		int x = 0;

		for(int i = 0; i < 6; i ++)
		{
			for(int j = 0; j < 6; j++)
			{
				if(playerNum == metriBordLine[i][j].player)
				{
					a[i][j] = metriBordLine[i][j].val;
				}
			}
		}

		for(int i = 0; i < 6; i++)
		{    
			for(int j = 0; j < 6; j++)
			{    
				for(int k = 0; k < 6; k++)
				{    
				x = a[i][k] * a[k][j]; 
				if(x ==1)
				{
					break;
				}
				} 
				c[i][j] = x;
			}    
		}      

		for(int i = 0; i < 6; i++)
		{    
			for(int j = 0; j < 6; j++)
			{    
				for(int k = 0; k < 6; k++)
				{    
				x = c[i][k] * a[k][j]; 
				if(x ==1)
				{
					break;
				}
				} 
				d[i][j] = x; 
			}    
		}      

		//multiplying and printing multiplication of 2 matrices    
		for(int i=0;i<6;i++){    
		for(int j=0;j<6;j++){    

		//System.out.print(d[i][j]+" ");  //printing matrix element  
		}//end of j loop  
		//System.out.println("==");//new line    
		} 
		for(int i = 0; i < 6; i++)
		{
			if(d[i][i]==1)
			{
				//System.out.println("win of player: " + playerNum);
				return 1;
			}
		}
		return 0;
	}

	public void paintComponent(Graphics g)
	{
		this.g = g;
		
		System.out.println("**********");
		for(int i = 0; i < numberLine; i++)
		{ 
			lArr[i].paint1(g);
			//System.out.println(lArr[i].colorLine);
		}


		cArr[0].paint1(g);
		cArr[1].paint1(g);
		cArr[2].paint1(g);
		cArr[3].paint1(g);
		cArr[4].paint1(g);
		cArr[5].paint1(g);
		//g.fillRect(10, 10, 50, 50);
		 
	}	

	void clearBord()
	{
		
		numberLine = 0;
		arrIndexCircle = new int[2];
		player = 1;
		curr = 0;
		addMouseListener(new ML());
		
		cArr = new CircleDrew [6];
		cArr[1] = new CircleDrew(50,320);
		cArr[4] = new CircleDrew(650,320);
		cArr[2] = new CircleDrew(200,10);
		cArr[3] = new CircleDrew(500,10);
		cArr[0] = new CircleDrew(200,650);
		cArr[5] = new CircleDrew(500,650);
		
		lArr = new LineDrew[50];
		metriBordLine = new Sell[6][6];

		//lArr[0] = new LineDrew(50, 370, 650, 370);
		
		for(int i = 0; i < 6; i++)
		{
			for(int j = 0; j < 6; j++)
			{
				if((metriBordLine[i][j] == null || metriBordLine[j][i] == null)	&& i != j)
				{
				
					metriBordLine[i][j] = new Sell(numberLine, i, j, i);
					metriBordLine[j][i] = metriBordLine[i][j];
					lArr[numberLine] = new LineDrew((cArr[i].getx() + 100/2), (cArr[i].gety() + 100/2), (cArr[j].getx() + 100/2), (cArr[j].gety() + 100/2));
					numberLine++;
				}
				if(i == j)
				{
					metriBordLine[i][j] = new Sell(0);
				}
			}
			
		}//System.out.println(numberLine);
	}

	public void printColorLine()
	{
		System.out.println("**********");
		for(int i = 0; i < 6; i++)
		{
			for(int j = 0; j < 6; j++)
			{
				int indexLine2 = metriBordLine[i][j].indexLine;
				if(indexLine2 != -1)
				{
					//System.out.println(lArr[indexLine2].colorLine);
				}
				
			}
			
		}
	}
	
	public Main()
	{
		f2=new JFrame();  
		option=Integer.parseInt(JOptionPane.showInputDialog(f2,"Enter option\n1 - player vs player\n2 - player vs bot"));  


		//System.out.println("player: " + option);

		numberLine = 0;
		arrIndexCircle = new int[2];
		player = 1;
		curr = 0;
		addMouseListener(new ML());
		
		cArr = new CircleDrew [6];
		cArr[1] = new CircleDrew(50,320);
		cArr[4] = new CircleDrew(650,320);
		cArr[2] = new CircleDrew(200,10);
		cArr[3] = new CircleDrew(500,10);
		cArr[0] = new CircleDrew(200,650);
		cArr[5] = new CircleDrew(500,650);
		
		lArr = new LineDrew[50];
		metriBordLine = new Sell[6][6];

		//lArr[0] = new LineDrew(50, 370, 650, 370);
		
		for(int i = 0; i < 6; i++)
		{
			for(int j = 0; j < 6; j++)
			{
				if((metriBordLine[i][j] == null || metriBordLine[j][i] == null)	&& i != j)
				{
					metriBordLine[i][j] = new Sell(numberLine, i, j, i);
					metriBordLine[j][i] = metriBordLine[i][j];
					lArr[numberLine] = new LineDrew((cArr[i].getx() + 100/2), (cArr[i].gety() + 100/2), (cArr[j].getx() + 100/2), (cArr[j].gety() + 100/2));
					numberLine++;
				}
				if(i == j)
				{
					metriBordLine[i][j] = new Sell(0);
				}
			}
			
		}//System.out.println(numberLine);

	}
  
	class ML extends MouseAdapter
	{
		public void mousePressed(MouseEvent e)
		{
			for(int k = 0; k < 6; k++)
			{
				/* ///////////////////////////////////////////////////////////////
				to show the click x and y									 /////
				System.out.println("Click at" + e.getX() + "," +e.getY());   /////
				//////////////////////////////////////*///////////////////////////
				
				//save the middel of circle:
				int a = (cArr[k].getx() + 100/2)-e.getX();
				int b = (cArr[k].gety() + 100/2)-e.getY();

				if( Math.sqrt(a*a + b*b) <= 100 ) //check if the click is inside
				{
					System.out.println("circel: " + k);
					curr++;
					if(curr == 2)// if the player click on 2 circle
					{
						arrIndexCircle[curr-1] = k;
						if(arrIndexCircle[0] == arrIndexCircle[1])
						{
							Main.this.repaint();
							curr = 0;
							arrIndexCircle[0] = 0;
							arrIndexCircle[1] = 0;
							return;
						}
						for(int i = 0; i < 6; i++) // pass all the metriBordLine 
						{
							for(int j = 0; j < 6; j++)
							{
								if(metriBordLine[i][j].val != -1) // must to fix the ultra condition!!! and merge them!!!
								{
									if(metriBordLine[i][j].val == 0 && metriBordLine[j][i].val == 0)
									{
										int w1 = metriBordLine[i][j].x1;
										int w2 = metriBordLine[i][j].x2;
										if(w1 == arrIndexCircle[0] || w2 == arrIndexCircle[0])
										{
											if(w1 == arrIndexCircle[1] || w2 == arrIndexCircle[1])
											{
												//this sell is not empty now 
												metriBordLine[i][j].val = 1;
												metriBordLine[j][i].val = 1;
												metriBordLine[i][j].player = player;
												int indexLine1 = metriBordLine[i][j].indexLine;//fix this line
												lArr[indexLine1].color(player);
												//System.out.println(indexLine1);

											}

										}

									}
								}
								
							}
						}
						Main.this.repaint();
						curr = 0;
						arrIndexCircle[0] = 0;
						arrIndexCircle[1] = 0;
						if(option == 1)
						{
							if(isWin(1) == 1)
							{
								System.out.println("blue player win");
								clearBord();
								Main.this.repaint();
								f2=new JFrame();  
								JOptionPane.showMessageDialog(f2,"player blue win!!!!");  
								f.dispatchEvent(new WindowEvent(f, WindowEvent.WINDOW_CLOSING));
		
								return ;
							}
							else if(isWin(2) == 1)
							{
								System.out.println("red player win");
								clearBord();
								Main.this.repaint();
								f3=new JFrame();  
								JOptionPane.showMessageDialog(f3,"player red win!!!!");  							
								f.dispatchEvent(new WindowEvent(f, WindowEvent.WINDOW_CLOSING));							
								return ;
							}
							if(player == 1)
							{
								player = 2;
							}
							else
							{
								player = 1;
							}

						}
						else
						{
							if(isWin(1) == 1)
							{
								System.out.println("blue player win");
								clearBord();
								Main.this.repaint();
								f2=new JFrame();  
								JOptionPane.showMessageDialog(f2,"player blue win!!!!");  
								f.dispatchEvent(new WindowEvent(f, WindowEvent.WINDOW_CLOSING));
		
								return ;
							}
							botPlayer();
							if(metriBordLine[0][1].val == 1 && metriBordLine[0][1].player == 2)
							{
								metriBordLine[0][1].val = 1;
								metriBordLine[1][0].val = 1;
								metriBordLine[0][1].player = 2;
								int indexLine1 = metriBordLine[0][1].indexLine;//fix this line
								lArr[indexLine1].color(2);
								Main.this.repaint();
							}
							else if(metriBordLine[0][1].val == 1 && metriBordLine[0][1].player == 1)
							{
								metriBordLine[0][1].val = 1;
								metriBordLine[1][0].val = 1;
								metriBordLine[0][1].player = 1;
								int indexLine1 = metriBordLine[0][1].indexLine;//fix this line
								lArr[indexLine1].color(1);
								Main.this.repaint();
							}
						}

					}
					else
					{
						arrIndexCircle[curr-1] = k;
					}
				}
			}
		}
	}	


  public static void main(String[] args)
  {
    f = new JFrame("Draw a circle");
    m = new Main();
    
    f.add(m);
    f.setSize(800, 800);
    f.setVisible(true);
    f.setResizable(false);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	if(option == 1 || option == 2)
	{	
	}
	else
	{
		f.dispatchEvent(new WindowEvent(f, WindowEvent.WINDOW_CLOSING));
	}
  }

}