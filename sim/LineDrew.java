import java.awt.Color;
import java.awt.Graphics;

public class LineDrew 
{
	public int x1;
	public int y1;
	public int x2;
	public int y2;
	public int flag = 1; 
	public int colorLine;
	
	public LineDrew(int x1, int y1, int x2, int y2)
	{
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		
	}
	
	 public void paint1(Graphics g)
	 {
	   if(1==flag && colorLine == 1)
	   {
		   
			g.setColor(Color.red);
			g.drawLine(x1, y1, x2, y2);
	   }
	   else if(1==flag && colorLine == 2)
	   {
			g.setColor(Color.blue);
			g.drawLine(x1, y1, x2, y2);
	   }
	   else
	   {
		   g.setColor(Color.BLACK);
		   g.drawLine(x1, y1, x2, y2);
	   }
	   
	}

	public void color(int color)
	 {
		colorLine = color;
	 }
}
